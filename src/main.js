import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
//
// new Vue({
//     router,
//     store,
//     el: '#app',
//     components: {App},
//     template: '<App/>'
// });
new Vue({el: '#app', router, store, render: h => h(App)});
